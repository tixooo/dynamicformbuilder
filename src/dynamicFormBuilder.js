import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';

function TextInput(props) {
    const { label, required, placeholder, value, onChange } = props;
    return (
        <div className="form-group">
            <label>{label} {required && <span className="text-danger">*</span>}</label>
            <input type="text" className="form-control" placeholder={placeholder} value={value} onChange={onChange} />
        </div>
    );
}
function Select(props) {
    const { label, required, value, options, onChange } = props;
    return (
        <div className="form-group">
            <label>{label} {required && <span className="text-danger">*</span>}</label>
            <select className="form-control" value={value} onChange={onChange}>
                {options.map(option => (
                    <option key={options.indexOf(option)} value={option.value}>{option.label}</option>
                ))}
            </select>
        </div>
    );
}
function Checkbox(props) {
    const { label, required, value, onChange } = props;
    return (
        <div className="form-check">
            <input type="checkbox" className="form-check-input" checked={value} onChange={onChange} />
            <label className="form-check-label">{label} {required && <span className="text-danger">*</span>}</label>
        </div>
    );
}
function DynamicForm(props) {
    const elements = props.formJSON.fields;
    const onSubmit = props.onSubmit;
    const [values, setValues] = useState({});
    const handleChange = (name, value) => {
        setValues(prevValues => ({
            ...prevValues,
            [name]: value
        }));
    };
    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit(values);
    };
    const renderElement = (element) => {
        const { type, id, label, required, placeholder, options } = element;
        const value = values[id] || '';
        switch (type) {
            case 'text':
                return (
                    <TextInput
                        key={id}
                        label={label}
                        required={required}
                        placeholder={placeholder}
                        value={value}
                        onChange={event => handleChange(id, event.target.value)}
                    />
                );
            case 'select':
                return (
                    <Select
                        key={id}
                        label={label}
                        required={required}
                        value={value}
                        options={options}
                        onChange={event => handleChange(id, event.target.value)}
                    />
                );
            case 'checkbox':
                return (
                    <Checkbox
                        key={id}
                        label={label}
                        required={required}
                        value={value}
                        onChange={event => handleChange(id, event.target.checked)}
                    />
                );
            default:
                return null;
        }
    };
    return (
        <form onSubmit={handleSubmit}>
            {elements.map(element => renderElement(element))}
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default DynamicForm;
import './App.css';
import formJSON from "./formData.json";
//import DynamicForm from "./dynamicFormBuilder";
import DynamicFormCC from "./dynamicFormBuilderCC";
import React from "react";

const handleFormSubmit = (values) => {
    console.log(values);
};

function App() {
    return (
        <div className="container">
            <h1>Dynamic Form Builder</h1>
            <DynamicFormCC formJSON={formJSON} onSubmit={handleFormSubmit}/>

        </div>
    )
}
export default App;